/*
 * Kodu5 - tähtaeg 6. April 2015
 * Node basics II https://www.youtube.com/watch?v=zSJRn8erf9M 
 * 				>> https://www.youtube.com/watch?v=9UJ2XtgRtMk >> 
 * 
 * indiakeelne https://www.youtube.com/watch?v=qH6yxkw0u78
 * 
 * recursion on linkedlists https://www.ics.uci.edu/~pattis/ICS-21/lectures/llrecursion/lecture.html
 * 							http://stackoverflow.com/questions/19738570/recursively-adding-node-to-end-of-linkedlist
 * 
 * http://enos.itcollege.ee/~jpoial/algoritmid/PuupraktikumNew.html
 * http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
 * https://bitbucket.org/i231/kodu5.git
 * 
 * mingi - https://www.seas.gwu.edu/~drum/cs1112/lectures/module10/suppl/index.html
 * 
 * Enda loeng: https://echo360.e-ope.ee/ess/echo/presentation/c15fbcdd-8541-44b3-9704-8bf6ac5f3e1d?ec=true @37min march12 2015
 * sügis2014 nõuanded https://echo360.e-ope.ee/ess/echo/presentation/7fc7adf8-ede3-4962-bb3d-8b2fb1f4227f?ec=true
 * sügis2014 teooria https://echo360.e-ope.ee/ess/echo/presentation/a510a537-7d5e-43c6-947f-3a2d06b0b2b0?ec=true
 * puu http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
 * 
 * Tree / Puu
 * Write a method to build a tree from the right parenthetic string representation 
 * (return the root node of the tree) and a method to construct the left parenthetic 
 * string representation of a tree represented by the root node this. 
 * Tree is a pointer structure with two pointers to link nodes of type  Node - pointer 
 * to the first child and pointer to the next sibling. Build test trees and print the 
 * results in main-method, do not forget to test a tree that consists of one node only. 
 * Node name must be non-empty and must not contain round brackets, commas and 
 * whitespace symbols. In case of an invalid input string the  parsePostfix method 
 * must throw a  RuntimeException with meaningful error message. 
 * 
 * public static Node parsePostfix (String s) 
 * public String leftParentheticRepresentation() 
 * 
 * 
 * Koostage meetodid puu parempoolse suluesituse (String) järgi puu moodustamiseks 
 * (parsePostfix, tulemuseks puu juurtipp) ning etteantud puu vasakpoolse suluesituse 
 * leidmiseks stringina (puu juureks on tipp  this, meetod leftParentheticRepresentation 
 * tulemust ei trüki, ainult tagastab String-tüüpi väärtuse). Testige muuhulgas ka 
 * ühetipuline puu. Testpuude moodustamine ja tulemuse väljatrükk olgu peameetodis. 
 * Puu kujutamisviisina kasutage viidastruktuuri (viidad "esimene alluv"  firstChild ja 
 * "parem naaber"  nextSibling, tipu tüüp on  Node). Tipu nimi ei tohi olla tühi ega 
 * sisaldada ümarsulge, komasid ega tühikuid. Kui meetodile  parsePostfix etteantav 
 * sõne on vigane, siis tuleb tekitada asjakohase veateatega  RuntimeException.
 * 
 */

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;

public class Node implements Iterator<Node> {

	private String name;
	private Node nextSibling; // TreeNode.java-st rightSibling asendatud sellega
	private Node firstChild;

	// teen muutujad while tsükli jaoks
	static String string = "";
	static String sibling = "";
	static String child = "";

	static StringBuilder sb = new StringBuilder();

	static Map<String, Node> map = new LinkedHashMap<String, Node>();

	static Stack<Node> nodes = new Stack<Node>(); // Node'ide push ja pop

	/**
	 * 
	 * @param n setName
	 * @param d setNextSibling
	 * @param r setFirstChild
	 */
	Node(String n, Node d, Node r) { // NAME, sibling d, child r
		// TODO!!! Your constructor here
		// following is from TreeNode.java
		setName(n);
		setNextSibling(d);
		setFirstChild(r);
	}
//		name = n;
//		nextSibling = d;
//		firstChild = r;

		
	///////// setters and getters ///////////////////////////////////////////////////////////////////////////////////////
	
		public void setName(String n) {						// setib nime
			name = n;
		}
		
		public String getName() {							// getib nime
			return name;
		}
		
		public void setNextSibling(Node d) {				// setib sibling'i
			nextSibling = d;
		}
		
		public Node getNextSibling() {						// getib silbing'i
			return nextSibling;
		}
		
		public void setFirstChild(Node r) {					// setib esimese child'i Node'i
			firstChild = r;
		}
		
		public Node getFirstChild() {						// tagastab esimese child'i
			return firstChild;
		}
		
   //////////////// 
		
	   @Override
	   public String toString() {
	      return leftParentheticRepresentation();
	   }
	
	   public void processNode() {							// sysoutib node'i nime
	      System.out.print (getName() + "  ");
	   }
	   
	   public boolean hasNext() {							// hasNext TRUE or FALSE
	      return (getNextSibling() != null);
	   }
	
	   public Node next() {									// next sibling?
	      return getNextSibling();
	   }
	
	   public void remove() {
	      throw new UnsupportedOperationException();
	   }
	
	   public Iterator<Node> children() {					// tagastab esimese child'i
	      return getFirstChild();
	   }
	
	   public void addChild (Node a) {						// lisab lapse
	      if (a == null) return;
	      Iterator<Node> children = children();
	      if (children == null)
	         setFirstChild (a);
	      else {
	         while (children.hasNext())
	            children = (Node)children.next();
	         ((Node)children).setNextSibling (a);
	      }
	   }
	
	   public boolean isLeaf() {							// isLeaf TRUE or FALSE
	      return (getFirstChild() == null);
	   }
	
	   public int size() {									// size ehk mitu elementi on puus
	      int n = 1; // root
	      Iterator<Node> children = children();
	      while (children != null) {
	         n = n + ((Node)children).size();
	         children = (Node)children.next();
	      }
	      return n;
	   }
	
	   public void preorder() {								// teeb puust preorderi
	      processNode();
	      Iterator<Node> children = children();
	      while (children != null) {
	         ((Node)children).preorder();					// rekursioon!!!
	         children = (Node)children.next();
	      }
	   }
	
	   public void postorder() {							// teeb puust postorderi
	      Iterator<Node> children = children();
	      while (children != null) {
	         ((Node)children).postorder();
	         children = (Node)children.next();
	      }
	      processNode();
	   }
	


	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	 * saab stringi, tagastab puu juure!
	 * tipu nimed ei tohi sisaldada sulge, komasid, ega whitespacesymbolit (tühik, tab)
	 * peab kontrollima, et string VASTAKS parempoolsesuluesituse struktuurile >> runtimeexception
	 * 
	 * ((B,C))A << see on vigane, peaks errori andma! siin tekiks tühja nimega tipp, see ei tohi tekkida C))A vahel
	 * sümbolihaaval ei tasu analüüsida stringi!
	 *  kasuta stringtokenizerit nii, et lõhub stringi (), järgi nii, et jätad characteri stringi sisse, saab teada mille järgi eraldati!!! (@7min)
	 */
	
//	public static Node tree(String s){
//		
//	}
   
	public static Node parsePostfix(String s) { 													// peab tagastama juure tipu
//		Node root = new Node ("A", null, new Node ("B1", new Node ("C", null, null), null));
//		Node root = new Node("A", null, null);
//		Node root = null;
//		Node ajut;
		String juur;
		Stack<String> list = new Stack<String>(); 													// ArrayList stringide jaoks kuhu läheb jupitatud sisend
		
		System.out.println("parsePostfix meetodi sisse tuli String s = " + s);
		
		if (s == null || s.isEmpty() || s.length() == 0) {
			throw new RuntimeException("Üritad parsida tühja stringi!");
		} else if (s.contains(" ")) {
			throw new RuntimeException("Üritad parsida stringi, milles on tühik! " + s);
		}
		else {
		}
		
		StringTokenizer st = new StringTokenizer(s, "(),", true);									// jupitab sisendtringi etteantud märkide (), kohapealt jättes märgid tulemusse sisse
		while (st.hasMoreTokens()) { 																// tühikud ära teeb trim! element on üks neljast võimalusest nüüd. Teeb elementidest listi
			String element = st.nextToken().trim(); 
			list.push(element);
			System.out.println("listi element: " + element + " ja list ise on " + list); 			// elem on nüüd üks neljast ehk "(" või "," või ")" või string (ehk tipu nimi)
		}
		
		for (int i = 0; i < list.size(); i++) {  													// sisuta sulud exception
			if (list.get(i).equals("(") && list.get(i + 1).equals(")")) {
				throw new RuntimeException("Üritad parsida stringi, milles on sisuta sulud! " + s + " märgid asukohtadel: " + i + " & " + (i+1));
			}
		}
		
		for (int i = 0; i < list.size(); i++) {  													// sisuta komad exception
			if (list.get(i).equals(",") && list.get(i + 1).equals(",")) {
				throw new RuntimeException("Üritad parsida stringi, milles on sisuta komad! Pane komade vahele miskit või eemalda koma! " + s + " märgid asukohtadel: " + i + " & " + (i+1));
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
//		Node nodeA = new Node("A", null, null);
//		Node nodeB = new Node("B", null, null);
//		Node nodeC = new Node("C", null, null);

		Node ajut;
		Node root;
		
		
		// lisab kõik node'id map'i
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(",") || list.get(i).equals("(") || list.get(i).equals(")")) {
			} else {
				ajut = new Node(list.get(i), null, null);
				map.put(ajut.getName(), ajut);
				System.out.println("map'i läks node nimega: " + list.get(i));
			}
		}

		
//		// otsib vasakult esimese nime, ehk root'i child'i
//		String rootchildname = null;
//		for (int i = 0; i < list.size(); i++) {
//			if (list.get(i).equals(",") || list.get(i).equals("(") || list.get(i).equals(")")) {
//			} else {
//				rootchildname = list.get(i);
//				break;
//			}
//		} // BS vist
		
		
		// loob root'i ehk viimane nimi
		root = new Node(list.pop(), null, null); // võtab viimase sümboli ja teeb sellest juure, määrab esimese lapse ka
		map.replace(root.getName(), root); // paneb root'i map'i
		System.out.println("listist popiti välja root ehk " + root.getName());
		
		// otsime vasakult alates viimase avava sulu positsiooni, kursor
		int kursorA = 0;
		int kursorB = 0;
		while (list.get(kursorA + 1).equals("(")) {
			kursorA = kursorA + 1;
		}
		while (list.get(kursorB).equals(")") == false) {
			kursorB = kursorB + 1;
		}
		System.out.println("kursorA : " + kursorA + " is " + list.get(kursorA+1)); // otsib viimase ( ja sellele järgneva tähe
		System.out.println("kursorB : " + kursorB + " is " + list.get(kursorB+1)); // otsib esimese ) ja selle järgneva tähe
		
		map.get(list.get(kursorB+1)).setFirstChild(map.get(list.get(kursorA+1))); // paneb esimesele ) järgnevale tähele esimese lapse, mis asub peale viimast (
		if (list.get(kursorA+2).equals(",")) { // kas ( järgneb G ja siis koma?
			map.get(list.get(kursorA+1)).setNextSibling(map.get(list.get(kursorA+2)));
		}

		list.remove(kursorA);
		list.remove(kursorA);
		list.remove(kursorA);
		list.remove(kursorA);
		list.remove(kursorA);
		
		System.out.println("uus list");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
		
		

//		System.out.println("juureks sai " + juur);

//		map.put("A", nodeA);
//		map.put("B", nodeB);
//		map.put("C", nodeC);

//		ajut = map.get("A");
//		ajut.setFirstChild(map.get("B"));
//		map.replace("A", ajut);
//
//		ajut = map.get("B");
//		ajut.setNextSibling(map.get("C"));
//		map.replace("B1", ajut);

//		while (list.empty() == false) {
//			string = list.pop();
//			System.out.println("string on " + string);
//			System.out.println("list on " + list);
//			if (string.equals("(")) {
//				
//			}
//		}
		
//		nodes.push(new Node("B1", null, null));
//		ajut = new Node("A", null, null);
//		nodes.push(ajut);
//		ajut = nodes.pop();
//		System.out.println("aju = " + ajut);
//		ajut.setFirstChild(new Node("B1", null, null));
//		ajut.setNextSibling(new Node("C", null, null));
//		nodes.push(ajut);
		//		ajut = nodes.pop(); System.out.println("ajut on " + ajut);
//		ajut.setName("foo");
//		nodes.push(ajut);
//		ajut = nodes.pop(); System.out.println("ajut on " + ajut);
//		ajut.setFirstChild(new Node("d", ajut.getFirstChild(), null));
//		nodes.push(new Node("B1", null, null));
//		ajut = nodes.pop(); System.out.println("ajut on " + ajut);
//		ajut.setFirstChild(new Node("f", ajut.getFirstChild(), null));
////		ajut.setName("G");
//		nodes.push(ajut);
//		System.out.println("nodes " + nodes);
//		System.out.println("now returning: " + root);
		return root; // TODO!!! return the root         new Node("A", null, null)
	}
	
//		int open = 1;
//		int close = 1;
//		for (int i = 0; i < list.size(); i++) {  // doublebrackets exception
//			if (list.get(i).equals("(") && list.get(i - 1).equals("(")) {
//				open = open + 1; 
//			}
//			if (list.get(i).equals(")") && list.get(i - 1).equals(")")) {
//				close = close + 1; 
//			}
//		}
//		if (open == close) {
//			throw new RuntimeException("Üritad parsida stringi, milles on topeltsulud " + s + " siin on " + open + " avavat ja sulgevat sulgu.");	
//		}

   
   public static void main (String[] param) {
	   
//	   String s = "(B1,C)A"; // (B1,C)A
//	   Node tt = Node.parsePostfix(s);
//	   String v = tt.leftParentheticRepresentation();
//	   System.out.println("peab olema: (B1,C)A ==> A(B1,C)");
//	   System.out.println("aga on:     " + s + " ==> " + v); // (B1,C)A ==> A(B1,C)

	   
	   // Number of nodes: 7
		// Postorder: B D E C G F A
		// Preorder!: A B C D E F G
		// A(B,C(D,E),F(G))
		// t.addChild(new Node("X", null, new Node("Y", null, null))); // lisatest, kui lisame node'e
		// t.addChild(new Node("X", null, new Node("Y", null, null))); // lisatest, kui lisame node'e

	   
	   // üritame lihtsat puud ehitada
	   
//	   Node root = new Node("A", null, null);
		Node nodeA = new Node("A", null, null);
		Node nodeB = new Node("B", null, null);
		Node nodeC = new Node("C", null, null);
		Node ajut;

		map.put("A", nodeA);
		map.put("B", nodeB);
		map.put("C", nodeC);

		ajut = map.get("A");
		ajut.setFirstChild(map.get("B"));
		map.replace("A", ajut);

		ajut = map.get("B");
		ajut.setNextSibling(map.get("C"));
		map.replace("B1", ajut);

		// Node t = createTree2();
		Node t = map.get("A");

//		System.out.println("prints first child " + map.get("A").firstChild);
		
		System.out.println("Number of nodes: " + String.valueOf(t.size()));
		System.out.print("Postorder: ");
		t.postorder();
		System.out.println();
		System.out.print("Preorder!: ");
		t.preorder();
		System.out.println();
		System.out.println("------- järgneb leftParentheticRepresentation ------");
		System.out.println(t.leftParentheticRepresentation());

	}

   // siia võib teha toString'i, mis leftParentheticRepresentation abil prindib välja 
   // (räägib kell 18.40 12March2015 - https://echo360.e-ope.ee/ess/echo/presentation/c15fbcdd-8541-44b3-9704-8bf6ac5f3e1d?ec=true) 
   
//	public void stringBuilder(String s) {
//		sb.append(s);
//		System.out.println("stringBuilder meetodis appendisin " + s + " ja sb on " + sb);
//	}
	
   
//////////////////////////// järgnev is done ///////////////////////////////////////////////////////////////////////////////////////////////////////
   
   
// tagstab String tüüpi asja, mis on see vasakpoolne suluesitus. Teksti moodustamine, lihtne.
// kui puu on olemas, käi preorderis läbi, õige koha peale sulud-komad, trüki tipunimed. 
// Tipud on Node-klassi objektid
// etteantud juurtipust alustades tekitada string A(B(C),D)
	public String leftParentheticRepresentation() { 									// TODO preorder. Tagastab StringBuilder.toString() String andmetüübi
		
		String temp = getName();														// võtab NODE'i nime!
		sb.append(temp);
		
		Iterator<Node> children = children();
		
		if (getFirstChild() != null) { sb.append("("); } 								//  avav sulg, kui on lapsi
//		if (getNextSibling() == null && getFirstChild() == null) { sb.append(")"); } 	//  sulud kinni, pole õdevend ja pole lapsi
		if (getFirstChild() == null && getNextSibling() != null) { sb.append(","); } 	//  koma, kui pole lapsi aga on õdevend
		
		while (children != null) {														// kui child on olemas, siis tee rekursioon!!!
			((Node) children).leftParentheticRepresentation();
			if (children.next() == null) {												//  sulud kinni, pole õdevend
				sb.append(")");
				if (this.hasNext() == true) { sb.append(","); }							// pane koma, kui oled siin kõige sees ja kui on next olemas (paneb sulgude järele koma)
			}
			children = (Node) children.next();
		}
		return sb.toString(); 
	}
   
   ///////////////// 3 näidispuud, mida pole enam vaja näppida /////////////////////////////////////////////////////////////////////////////////////
   
   public static Node createTree() {					// loob järgneva struktuuriga puu (NodeTree näide)
	   Node root = new Node ("+", null, 
			   new Node ("*",
					   new Node ("/", null,
							   new Node ("6",
									   new Node ("3", null, null),
									   null)),
									   new Node ("-",
											   new Node ("4", null, null),
											   new Node ("2",
													   new Node ("1", null, null),
													   null))));     
	   return root;
   }
   
   public static Node createTree2() {					// loob järgneva struktuuriga puu (lihtsaim)
	   Node root = new Node ("A", null, 
			   new Node ("B1",
					   new Node ("C", null, null), null));     
	   return root;
   }
   
   public static Node createTree3() {					// loob järgneva struktuuriga puu (loengu näide)
		Node root = new Node("A", null, new Node("B", new Node("C", new Node("F", null, new Node("G", null, null)), new Node("D", new Node("E", null, null), null)), null));
		return root;
	}
}
