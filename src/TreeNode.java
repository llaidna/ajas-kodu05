
import java.util.*;

public class TreeNode implements java.util.Iterator<TreeNode> {

   private String name;
   private TreeNode rightSibling;
   private TreeNode firstChild;
//   private int info;

   TreeNode (String s, TreeNode p, TreeNode a) {
      setName (s);
      setRightSibling (p);
      setFirstChild (a);
   }

/*
//   TreeNode (String s, TreeNode p, TreeNode a, int i) {
//      setName (s);
//      setRightSibling (p);
//      setFirstChild (a);
//      setInfo (i);
//   }

//   TreeNode() {
//      this ("", null, null, 0);
//   }
//
//   TreeNode (String s) {
//      this (s, null, null, 0);
//   }
//
//   TreeNode (String s, TreeNode p) {
//      this (s, p, null, 0);
//   }
*/

   public void setName(String s) {
      name = s;
   }

   public String getName() {
      return name;
   }

   public void setRightSibling (TreeNode p) {
      rightSibling = p;
   }

   public TreeNode getRightSibling() {
      return rightSibling;
   }

   public void setFirstChild (TreeNode a) {
      firstChild = a;
   }

   public TreeNode getFirstChild() {
      return firstChild;
   }

   /*
//   public void setInfo (int i) {
//      info = i;
//   }
//
//   public int getInfo() {
//      return info;
//   }
 */

   @Override
   public String toString() {
      return leftParentheticRepresentation();
   }

   public void processTreeNode() {
      System.out.print (getName() + "  ");
   }

   public boolean hasNext() {
      return (getRightSibling() != null);
   }

   public TreeNode next() {
      return getRightSibling();
   }

   public void remove() {
      throw new UnsupportedOperationException();
   }

   public Iterator<TreeNode> children() {
      return getFirstChild();
   }

   public void addChild (TreeNode a) {
      if (a == null) return;
      Iterator<TreeNode> children = children();
      if (children == null)
         setFirstChild (a);
      else {
         while (children.hasNext())
            children = (TreeNode)children.next();
         ((TreeNode)children).setRightSibling (a);
      }
   }

   public boolean isLeaf() {
      return (getFirstChild() == null);
   }

   public int size() {
      int n = 1; // root
      Iterator<TreeNode> children = children();
      while (children != null) {
         n = n + ((TreeNode)children).size();
         children = (TreeNode)children.next();
      }
      return n;
   }

   public void preorder() {
      processTreeNode();
      Iterator<TreeNode> children = children();
      while (children != null) {
         ((TreeNode)children).preorder();
         children = (TreeNode)children.next();
      }
   }

   public void postorder() {
      Iterator<TreeNode> children = children();
      while (children != null) {
         ((TreeNode)children).postorder();
         children = (TreeNode)children.next();
      }
      processTreeNode();
   }

   public String leftParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      b.append (getName());
      // TODO!!!
      return b.toString();
   }

   public String rightParentheticRepresentation() {
      StringBuffer b = new StringBuffer();
      // TODO!!!
      return b.toString();
   }
 
   public static TreeNode createTree() {
      TreeNode root = new TreeNode ("+", null, 
         new TreeNode ("*",
            new TreeNode ("/", null,
               new TreeNode ("6",
                  new TreeNode ("3", null, null),
                  null)),
            new TreeNode ("-",
               new TreeNode ("4", null, null),
               new TreeNode ("2",
                  new TreeNode ("1", null, null),
                  null))));     
      return root;
   }

   public static void main (String[] param) {
      TreeNode t = createTree();
      System.out.println ("Number of nodes: " + String.valueOf (t.size()));
      System.out.print ("Preorder: ");
      t.preorder();
      System.out.println();
      System.out.print ("Postorder: ");
      t.postorder();
      System.out.println();
   } // main

} // TreeNode

